package com.sample.app.ui;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.app.R;
import com.sample.app.model.Movie;
import com.sample.app.utils.ImageUtil;

public class DetailActivity extends AppCompatActivity {


    private TextView mTitle;
    private TextView mYearRelease;
    private TextView mRatings;
    private TextView mOverview;
    private ImageView mBackdrop;
    private ImageView mCover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Movie Details");

        mTitle = (TextView)  findViewById(R.id.movieTitle);
        mYearRelease = (TextView)  findViewById(R.id.yearRelease);
        mRatings = (TextView)  findViewById(R.id.ratings);
        mOverview = (TextView)  findViewById(R.id.overview);
        mBackdrop = (ImageView)  findViewById(R.id.backdrop);
        mCover = (ImageView)  findViewById(R.id.cover);

        Bundle bundle = getIntent().getExtras();
        Movie movie = (Movie) bundle.getSerializable("movie");
        Log.d("TAG", movie.toString());

        populatedView(movie);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populatedView(Movie movie){
        mTitle.setText(movie.getTitle());
        mYearRelease.setText(movie.getYear());
        mRatings.setText(movie.getRating());
        mOverview.setText(movie.getOverview());

        mCover.setImageBitmap(ImageUtil.getImageBitmapThumbnail(Environment.getExternalStorageDirectory().toString()
                .concat("/sampleApp/")+movie.getSlug()+"-cover.jpg"));

        mBackdrop.setImageBitmap(ImageUtil.getImageBitmapThumbnail(Environment.getExternalStorageDirectory().toString()
                .concat("/sampleApp/")+movie.getSlug()+"-backdrop.jpg"));
    }
}
