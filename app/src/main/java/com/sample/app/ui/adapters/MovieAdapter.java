package com.sample.app.ui.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sample.app.R;
import com.sample.app.model.Movie;
import com.sample.app.utils.ImageUtil;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;


public class MovieAdapter extends BaseAdapter {

    private Context mContext;
    protected LayoutInflater inflater;
    private ArrayList<Movie> mMovieList;

    public MovieAdapter(Context context, ArrayList<Movie> movieList){
        this.mContext = context;
        this.mMovieList = movieList;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return mMovieList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMovieList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder mHolder = null;
        if (mHolder == null) {
            view = inflater.inflate(R.layout.movie_list_item, null);
            mHolder = new ViewHolder();
            view.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) view.getTag();
        }

        mHolder.title = (TextView) view.findViewById(R.id.movieTitle) ;
        mHolder.year = (TextView) view.findViewById(R.id.yearRelease) ;
        mHolder.imgBackdrop = (ImageView) view.findViewById(R.id.backdrop);


        mHolder.title.setText(mMovieList.get(position).getTitle());
        mHolder.year.setText(mMovieList.get(position).getYear());

        mHolder.imgBackdrop.setImageBitmap(ImageUtil.getImageBitmapThumbnail(
                Environment.getExternalStorageDirectory().toString()
                        .concat("/sampleApp/")+mMovieList.get(position).getSlug()+"-backdrop.jpg"));


        return view;
    }


    private class ViewHolder {
        TextView title, year;
        ImageView imgBackdrop;
    }

}
