package com.sample.app.model;

import java.io.Serializable;
import java.util.Arrays;

public class Movie implements Serializable {

    private int id;
    private String rating;
    private String[] genres;
    private String language;
    private String title;
    private String url;
    private String titleLong;
    private String imdbCode;
    private String state;
    private String year;
    private int runtime;
    private String overview;
    private String slug;
    private String mpaRating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitleLong() {
        return titleLong;
    }

    public void setTitleLong(String titleLong) {
        this.titleLong = titleLong;
    }

    public String getImdbCode() {
        return imdbCode;
    }

    public void setImdbCode(String imdbCode) {
        this.imdbCode = imdbCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getMpaRating() {
        return mpaRating;
    }

    public void setMpaRating(String mpaRating) {
        this.mpaRating = mpaRating;
    }

    public String[] getGenres() {
        return genres;
    }

    public void setGenres(String[] genres) {
        this.genres = genres;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", rating='" + rating + '\'' +
                ", genres=" + Arrays.toString(genres) +
                ", language='" + language + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", titleLong='" + titleLong + '\'' +
                ", imdbCode='" + imdbCode + '\'' +
                ", state='" + state + '\'' +
                ", year='" + year + '\'' +
                ", runtime=" + runtime +
                ", overview='" + overview + '\'' +
                ", slug='" + slug + '\'' +
                ", mpaRating='" + mpaRating + '\'' +
                '}';
    }
}
