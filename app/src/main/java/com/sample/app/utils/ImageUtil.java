package com.sample.app.utils;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;

public class ImageUtil {

    public static Bitmap getImageBitmapThumbnail(String path){
        File file = new File(path);
        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
        return bm;
    }
}
