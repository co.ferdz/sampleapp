package com.sample.app.utils;

public class Common {

    private static final String BASE_URL = "http://cayaco.info/movielist";
    public static final String DATA_URL = BASE_URL + "/list_movies_page1.json";

    public static final String BACKDROP_IMAGE_URL = BASE_URL + "/images/%s-backdrop.jpg";
    public static final String COVER_IMAGE_URL = BASE_URL + "/images/%s-cover.jpg";
}
