package com.sample.app.request;


import java.util.HashMap;

public interface RequestTask {
    String getUrl();
    String getName();
    String getProgressText();
    void executeRequest();
    void setTaskListener(RequestTaskListener listener);
    HashMap<String, String> getCustomHeader();
}
