package com.sample.app.request;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sample.app.utils.volley.MyVolley;

import org.json.JSONObject;

import java.util.HashMap;

public abstract class AbstractRequestTask implements RequestTask {

    private Context context;
    private int requestMethod = Request.Method.POST;
    protected RequestTaskListener taskListener;


    @Override
    public void setTaskListener(RequestTaskListener taskListener) {
        this.taskListener = taskListener;
    }

    public AbstractRequestTask(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public void executeRequest() {
        taskListener.updateProgress(getProgressText());
        RequestQueue mReqQueue = MyVolley.getRequestQueue();
        JsonObjectRequest jsonReq = new JsonObjectRequest(getRequestMethod(), getUrl(), getJSONParams(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject obj) {
                        handleResponse(obj);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if(volleyError.networkResponse != null){
                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                        volleyError = new VolleyError(new String("Connection timeout : " + volleyError.networkResponse.statusCode));
                    } else if (volleyError instanceof AuthFailureError) {
                        volleyError = new VolleyError(new String("Authentication error : " + volleyError.networkResponse.statusCode));
                    } else if (volleyError instanceof ServerError) {
                        volleyError = new VolleyError(new String("Server error : " + volleyError.networkResponse.statusCode));
                    } else if (volleyError instanceof NetworkError) {
                        volleyError = new VolleyError(new String("Network error : " + volleyError.networkResponse.statusCode));
                    } else if (volleyError instanceof ParseError) {
                        volleyError = new VolleyError(new String("Parse error : " + volleyError.networkResponse.statusCode));
                    }
                } else {
                    volleyError = new VolleyError(new String("Server is not reachable, please check your network connection."));
                }
                handleErrorResponse(volleyError);
            }
        });

        mReqQueue.add(jsonReq);
    }

    public int getRequestMethod() {
        return requestMethod;
    }

    @Override
    public HashMap<String, String> getCustomHeader() {
        return null;
    }

    abstract JSONObject getJSONParams();

    abstract void handleResponse(JSONObject response);

    abstract void handleErrorResponse(VolleyError error);
}
